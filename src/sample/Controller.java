package sample;

import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.applet.AudioClip;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.logging.Logger;


public class Controller {

    @FXML
    public Rectangle lamp1, lamp2, lamp3, lamp4;

    @FXML
    public ProgressIndicator soundIndicator;

    @FXML
    public Rectangle redLine;

    @FXML
    public Slider volume;

    @FXML
    public RadioButton buttonD, buttonU;

    Double dX = 0.0;
    Boolean isOn = false, isPlaying = false;

    Media media1 = new Media(Paths.get("classic.mp3").toUri().toString());
    MediaPlayer mediaPlayer1 = new MediaPlayer(media1);

    Media media2 = new Media(Paths.get("violin.mp3").toUri().toString());
    MediaPlayer mediaPlayer2 = new MediaPlayer(media2);

    Media media3 = new Media(Paths.get("piano.mp3").toUri().toString());
    MediaPlayer mediaPlayer3 = new MediaPlayer(media3);

    Media media4 = new Media(Paths.get("guitar.mp3").toUri().toString());
    MediaPlayer mediaPlayer4 = new MediaPlayer(media4);

    Boolean music1playing = mediaPlayer1.getStatus().equals(MediaPlayer.Status.PLAYING);
    Boolean music2playing = mediaPlayer2.getStatus().equals(MediaPlayer.Status.PLAYING);
    Boolean music3playing = mediaPlayer3.getStatus().equals(MediaPlayer.Status.PLAYING);
    Boolean music4playing = mediaPlayer4.getStatus().equals(MediaPlayer.Status.PLAYING);




    @FXML
    public void setSoundIndicatorLeft(MouseEvent mouseEvent) {

        Double xValue = (mouseEvent.getX() - dX) / 100.0;

        Double temp = soundIndicator.getProgress() + xValue;

        if (temp > 0.9999) {
            temp = 0.9999;
        }
        if (temp < 0.001) {
            temp = 0.001;
        }

        soundIndicator.setProgress(temp);

        redLine.setX(temp * 480);
        dX = mouseEvent.getX();
        System.out.println(redLine.getX());

        playMusic();


    }

    @FXML
    public void setOn() {

        Color yellow = Color.rgb(251, 248, 179);
        Color grey = Color.rgb(230, 235, 239);

        if (!isOn) {
            lamp1.setFill(yellow);
            lamp2.setFill(yellow);
            lamp3.setFill(yellow);
            lamp4.setFill(yellow);
            isOn = true;
        } else if (isOn) {
            lamp1.setFill(grey);
            lamp2.setFill(grey);
            lamp3.setFill(grey);
            lamp4.setFill(grey);
            isOn = false;
        }

        playMusic();


    }

    @FXML
    public void playMusic() {

        //switch on music
        if(isOn && redLine.getX() > 30 && redLine.getX() < 120 && buttonU.isSelected() && !music1playing){
            mediaPlayer1.play();
        }
        if (isOn && redLine.getX() > 150 && redLine.getX() < 270 && buttonU.isSelected() && !music2playing) {
            mediaPlayer2.play();
        }
        if (isOn && redLine.getX() > 0 && redLine.getX() < 70 && buttonD.isSelected() && !music3playing) {
            mediaPlayer3.play();
        }
        if (isOn && redLine.getX() > 200 && redLine.getX() < 300 && buttonD.isSelected() && !music4playing) {
            mediaPlayer4.play();
        }

        //switch off music
        if(redLine.getX() < 30 || redLine.getX() > 120 || !buttonU.isSelected() || !isOn){
            mediaPlayer1.pause();
        }
        if (redLine.getX() < 150 || redLine.getX() > 270 || !buttonU.isSelected() || !isOn){
            mediaPlayer2.pause();

        }
        if (redLine.getX() < 0 || redLine.getX() > 70 || !buttonD.isSelected() || !isOn){
            mediaPlayer3.pause();

        }
        if (redLine.getX() < 200 || redLine.getX() > 300 || !buttonD.isSelected() || !isOn){
            mediaPlayer4.pause();

        }


    }

    @FXML
    public void setVolume(){
            mediaPlayer1.setVolume(volume.getValue()/100);
            mediaPlayer2.setVolume(volume.getValue()/100);
            mediaPlayer3.setVolume(volume.getValue()/100);
            mediaPlayer4.setVolume(volume.getValue()/100);
            System.out.println(volume.getValue());
    }



}
